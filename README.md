EXAMEN PEREYRA IGNACIO

Parcial de Tecnología de la Información

Acceda mediante Putty y WinSCP, a la máquina “Centos7” del VirtualBox local.
Instale GIT y Docker en la máquina a la que ha accedido, si corresponde.
Construya en ella un Stack con los siguientes contenedores de Docker:
app: con PHP 7.2 y Apache 2.4. Para acceder al puerto 80 de este contenedor, utilice el 87 del Host.
db: con MySQL 8. Para acceder al puerto 3306 de este contenedor, utilice el 3313 del Host.
Aloje una página PHP de prueba y verifique, desde un navegador, que sea servida correctamente por el contenedor “app”.
Sustituya a la página PHP de prueba, por el código fuente correspondiente a la aplicación “Clasifiautos”, que se encuentra en https://drive.google.com/open?id=11sdJe5KP2RZlGkRqB8rNvja3-iXvcL5W.
Cree la base de datos “clasifiautos_db”, con el juego de caracteres UTF-8.
Realice el “restore” del archivo de Dump llamado “clasifiautos_db.sql”, para regenerar los contenidos de la base de datos “clasifiautos_db”.
Edite el archivo “configuracion.inc.php” para establecer las credenciales de conexión con la base de datos.
Pruebe a acceder a la página “index.php” de la raíz del sitio Web, desde un navegador. Verifique su correcto funcionamiento y el de las restantes secciones de la aplicación.
Utilice variables de entorno en el contenedor “app” y haga uso de las mismas mediante el comando “sed” de Linux, durante el Building de los contenedores, para sustituir las credenciales de conexión con la base de datos, que se encuentran en el archivo “configuracion.inc.php”, por valores procedentes de Environment.
Efectúe nuevamente el Building de los contenedores, para verificar la correcta realización de lo planteado en el punto anterior.
Cree un repositorio GIT en su cuenta de Bitbucket.
Realice un “add” y un “commit” de GIT, que abarquen a todos los cambios realizados. Finalmente, realice un “push” al repositorio recientemente creado.
Otorgue al docente un acceso de sólo lectura al repositorio.
